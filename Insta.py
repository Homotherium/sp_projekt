import os
import errno
from urllib.request import *
from imageai.Detection import ObjectDetection

import requests


# Methode zum erstellen von Ordnern
def require_dir(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise


# Methode zum konvertieren von Zahlen
def roundAbos(zahl):
    if zahl > 1000000:
        return str(zahl / 1000000)[:4] + "m"
    elif zahl > 1000:
        return str(zahl)[:-3] + "k"
    else:
        return zahl


# Methode zum Abrufen von Kontoinformationen
def getAcoutInfo(data):
    print("")
    print(f"Accoutname : {data['graphql']['user']['username']}")
    print(f"Anzahl von Beiträge : {data['graphql']['user']['edge_owner_to_timeline_media']['count']}")
    print(f"Anzahl von Abonnenten : {roundAbos(data['graphql']['user']['edge_followed_by']['count'])}")
    print(f"Anzahl von abonnierten Accounts : {data['graphql']['user']['edge_follow']['count']}")
    print("")


# Verbindung mit der Instagram-Website und Abrufen von Daten als JSON-Datei
def connect():
    insta_url = 'https://www.instagram.com/'
    insta_username = input('Geben Sie ein Insta-Account ein : ')
    url = insta_url + insta_username

    querystring = {"__a": "1"}

    headers = {
        'User-Agent': "PostmanRuntime/7.18.0",
        'Accept': "*/*",
        'Cache-Control': "no-cache",
        'Postman-Token': "a4480f5e-c88e-4a2d-b58d-551da945ba71,ae38e024-3441-4afe-8c3a-77d1ad665c6a",
        'Accept-Encoding': "gzip, deflate",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)
    return response.json()


# Foto herunterladen
def download_photos(data):
    exec_path = os.getcwd()
    directory = os.path.join(exec_path, data['graphql']['user']['username'] + "/orig")
    require_dir(directory)
    print("Fange Fotos herunterzuladen")
    for image in data['graphql']['user']['edge_owner_to_timeline_media']['edges']:
        filename = os.path.join(directory, image['node']['shortcode'] + '.jpg')
        urlretrieve(image['node']['thumbnail_src'], filename)
    print("Herunterzuladen ist beendet!")


# Erkennung von Objekten
def detection(name):
    print("Fange Objekten auf Fotos zu erkennen")
    exec_path = os.getcwd()
    detector = ObjectDetection()
    detector.setModelTypeAsRetinaNet()
    detector.setModelPath(os.path.join(exec_path, "resnet50_coco_best_v2.0.1.h5"))
    detector.loadModel()
    directory = os.path.join(exec_path, name)
    files = os.listdir(directory + "/orig")
    images = filter(lambda x: x.endswith('.jpg'), files)
    require_dir(directory + "/mod")
    for image in images:
        detector.detectObjectsFromImage(
            input_image=os.path.join(directory + "/orig", image),
            output_image_path=os.path.join(directory + "/mod", image),
            minimum_percentage_probability=60,
            display_percentage_probability=False,
            display_object_name=True
        )
    print("Erkennungsprozess ist beendet!")


# Die Hauptmethode, die alle anderen aufruft
def main():
    data = connect()
    getAcoutInfo(data)
    download_photos(data)
    detection(data['graphql']['user']['username'])


main()
